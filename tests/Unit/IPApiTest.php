<?php

namespace Tests\Unit;

use App\Location;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IPApiTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_ip_api_returns_location()
    {
        $location = Location::fromIp('90.181.50.125');

        $this->assertEquals($location->latitude, 50.0833);
        $this->assertEquals($location->longitude, 14.2667);
    }

    public function test_local_ip_address_throws_exception()
    {
        $this->expectExceptionMessage('IP api fail');

        $location = Location::fromIp('127.0.0.1');
    }

    public function test_location_returns_default_location()
    {
        $location = Location::default();

        $this->assertEquals($location->latitude, 50.075538);
        $this->assertEquals($location->longitude, 14.437800);
    }

    public function test_ip_api_handles_invalid_ip()
    {
        $this->expectExceptionMessage('IP api fail');

        $location = Location::fromIp('hello there');
    }
}
