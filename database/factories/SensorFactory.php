<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Sensor;
use Faker\Generator as Faker;

$factory->define(Sensor::class, function (Faker $faker) {
    return [
        'name' => $faker->optional()->words(2, true),
        'air_temperature' => $faker->randomFloat(2, 20, 40),
        'road_temperature' => $faker->randomFloat(2, 30, 50),
        'humidity' => $faker->randomFloat(2, 0, 100),
        'wind_direction' => $faker->numberBetween(0, 360),
        'wind_speed' => $faker->randomFloat(2, 0, 10),
        'latitude' => $faker->randomFloat(6, 0.00001, 0.05) + 50.075538,
        'longitude' => $faker->randomFloat(6, 0.00001, 0.05) + 14.437800
    ];
});
