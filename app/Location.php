<?php

namespace App;

class Location
{
    public $latitude;
    public $longitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public static function fromIp(string $ip)
    {
        $response = file_get_contents('http://ip-api.com/json/' . urlencode($ip));

        if (!$response) {
            throw new \Exception('IP api fail');
        }

        $data = json_decode($response);

        if (empty($data->lat) || empty($data->lon)) {
            throw new \Exception('IP api fail');
        }

        return new static($data->lat, $data->lon);
    }

    public static function fromGps(string $gps)
    {
        if (!preg_match('/^\d{1,3}\.?\d*\|\d{1,3}\.?\d*$/', $gps)) return null;
        
        $exploded = explode('|', $gps);

        return new static($exploded[0], $exploded[1]);
    }

    public static function default()
    {
        // Prag center location
        return new static(50.075538, 14.437800);
    }

    public function distanceFormula()
    {
        return "acos (sin({$this->latitude}) * sin(latitude) + cos({$this->latitude}) * cos(latitude) * cos({$this->longitude} - longitude) ) * 111190";
    }
}