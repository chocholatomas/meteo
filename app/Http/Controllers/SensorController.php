<?php

namespace App\Http\Controllers;

use App\Sensor;
use App\Location;
use Illuminate\Http\Request;

class SensorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $location = null;

        if ($request->has('gps'))
        {
            $location = Location::fromGps($request->gps);
        }

        if (!$location) {
            try {
                $location = \Cache::remember($request->ip(), 60*60*24, function () use ($request) {return Location::fromIp($request->ip());});
            } catch (\Exception $e) {
                $location = Location::default();
            }
        }

        
        $availableSorts = [
            'humidity' => 'humidity',
            'air' => 'air_temperature',
            'road' => 'road_temperature',
            'wind' => 'wind_speed',
            'distance' => 'distance'
        ];

        $sorts = explode(',', $request->input('sort', ''));

        $radius = floatval($request->input('radius', 5000));
        $radius = $radius ?: 5000;

        $sensors = Sensor::withinRadius($location, $radius);

        foreach ($sorts as $sort) {
            if (array_key_exists(ltrim($sort, '-'), $availableSorts)) {
                $direction = starts_with($sort, '-') ? 'desc' : 'asc';

                $sensors->orderBy($availableSorts[ltrim($sort, '-')], $direction);
            }
        }

        return $sensors->get();
    }
}
