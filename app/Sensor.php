<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $fillable = [
        'name',
        'air_temperature',
        'road_temperature',
        'humidity',
        'wind_direction',
        'wind_speed',
        'latitude',
        'longitude'
    ];

    protected $casts = [
        'air_temperature' => 'decimal:2',
        'road_temperature' => 'decimal:2',
        'humidity' => 'decimal:2',
        'wind_direction' => 'integer',
        'wind_speed' => 'decimal:2',
        'latitude' => 'decimal:6',
        'longitude' => 'decimal:6'
    ];

    public function scopeWithinRadius($query, Location $location, int $meters)
    {
        return $query->select('*')
            ->selectRaw(
                '(' .
                $location->distanceFormula() .
                ') as distance'
            )->having('distance', '<=', $meters);
    }
}
